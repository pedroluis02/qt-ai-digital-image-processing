#include "myimagesop.h"

MyImagesOp::MyImagesOp(QString url_image) {
    rgb = true;
    if(!url_image.isEmpty()) {
        loadMyImage(url_image);
    }
}
void MyImagesOp::loadMyImage(QString url_image) {
    imageOR.load(url_image);

    if(imageOR.isGrayscale()) {
        qDebug() << "Gray Scale";
        rgb = false;
    }

    QImage imge1(imageOR.width(), imageOR.height(), imageOR.format());
    imageE1 = imge1;
    QImage imge2(imageOR.width(), imageOR.height(), imageOR.format());
    imageE2 = imge2;
    QRgb pixel; int r, g, b, p;
    for (int i = 0; i < imageOR.width(); i++) {
        for (int j = 0; j < imageOR.height(); j++) {
            if(rgb) {
                pixel = imageOR.pixel(i, j);
                r = qRed(pixel); g = qGreen(pixel); b = qBlue(pixel);
                p = (r + g + b) / 3;
                imageE1.setPixel(i, j, qRgb(p, p, p));

                p = (r * 0.3) + (g * 0.59) + (b * 0.11);
                imageE2.setPixel(i, j, qRgb(p, p, p));

            } else {
                p = qGray(imageOR.pixel(i, j));
                imageE1.setPixel(i, j, qRgb(p, p, p));
                imageE2.setPixel(i, j, qRgb(p, p, p));
            }
        }
    }
}
QImage MyImagesOp::orImage() {
    return imageOR;
}
QImage MyImagesOp::grayE1Image() {
    return imageE1;
}
QImage MyImagesOp::grayE2Image() {
    return imageE2;
}
// gray image
QImage MyImagesOp::binarizedImage(QImage img_in, int umbral) {
    int p;
    QImage newImage(img_in.width(), img_in.height(), img_in.format());
    for (int i = 0; i < img_in.width(); i++) {
        for (int j = 0; j < img_in.height(); j++) {
            p = qGray(img_in.pixel(i, j));
            if(p < umbral) {
                newImage.setPixel(i, j, qRgb(0, 0, 0));
            } else {
                newImage.setPixel(i, j, qRgb(255, 255, 255));
            }
        }
    }
    return newImage;
}
int * MyImagesOp::histogramImage(QImage img, int iw, int ih, int fw, int fh) {
    int length; bool gray = false;
    if(img.isGrayscale()) {
        length = 256;
        gray = true;
    } else {
        length = (256 * 3);
    }
    int * hist = new int[length];
    for (int var = 0; var < length; var++) {
        hist[var] = 0;
    }
    int p, r, g, b;
    for (int i = iw; i <= fw; i++) {
        for (int j = ih; j <= fh; j++) {
            if(gray) {
               p = qGray( img.pixel(i, j) );
               hist[p] += 1;
            } else {
                p =  img.pixel(i, j);
                r = qRed(p);
                g = qGreen(p);
                b = qBlue(p);
                hist[r] += 1;
                hist[g + 256] += 1;
                hist[b + 512] += 1;
            }
        }
    }
    return hist;
}
int MyImagesOp::umbralImageRegion(int *histogram) {
    int i = 0, j = 255;
    int maxRight = 0, maxLeft = 0, indexR = -1, indexL = -1;
    while(i < 256) {
        if(histogram[i] > maxLeft && i != indexR) {
            maxLeft = histogram[i];
            indexL = i;
        }

        if(histogram[j] > maxRight && j != indexL) {
            maxRight = histogram[j];
            indexR = j;
        }
       i++; j--;
    }

    if(indexL > indexR) {
        qSwap(indexL, indexR);
    }
    return minIndexArray(histogram, indexL + 1, indexR - 1);
}

QImage MyImagesOp::binarized_P_Image(QImage img_in, int p) {
    QImage newImage = img_in.copy();
    QList <QuadrantImg> listQuadrants = quadrantsImage(img_in, p);
    QuadrantImg q;
    int umbral;
    foreach (q, listQuadrants) {
        int * histogram = histogramImage(img_in,
                                         q.first_width, q.first_height, q.last_width, q.last_height);
        umbral = umbralImageRegion(histogram);
        binarized_rect(newImage,
                       q.first_width, q.first_height, q.last_width, q.last_height,
                       umbral);
        delete []histogram;
    }

    return newImage;
}

void MyImagesOp::binarized_rect(QImage &image_in, int iw, int ih, int fw, int fh, int umbral) {
    int p;
    for (int i = iw; i <= fw; i++) {
        for (int j = ih; j <=fh ; ++j) {
            p = qGray(image_in.pixel(i, j));
            if(p < umbral) {
                image_in.setPixel(i, j, qRgb(0, 0, 0));
            } else {
                image_in.setPixel(i, j, qRgb(255, 255, 255));
            }
        }
    }
}

QList<QuadrantImg> MyImagesOp::quadrantsImage(QImage img_in, int partitions) {
    int wp = qRound( (float)img_in.width() / (float)partitions),
        hp = qRound( (float)img_in.height() / (float)partitions);

    QList <QuadrantImg> listQuadrants;
    int p = partitions - 1;

    int i = 0, j = 0, fw, fh, lw, lh;
    QuadrantImg quadrant;
    while(i < partitions) {
        fw = i * wp + i;
        if(i == p) {
            lw = img_in.width() - 1;
        } else {
            lw = fw + wp;
        }
        while(j < partitions) {
            fh = j * hp + j;
            if(j == p) {
                lh = img_in.height() - 1;
            } else {
                lh = fh + hp;
            }
            j++;

            quadrant.first_width = fw; quadrant.last_width = lw;
            quadrant.first_height = fh; quadrant.last_height = lh;

            listQuadrants.append(quadrant);
        }
        j = 0;
        i++;
    }
    return listQuadrants;
}

// filter 3 x 3
QImage MyImagesOp::filterImage(QImage img, qreal F[3][3], qreal pf) {
    QImage newImage(img.width(), img.height(), img.format());
    qreal filter; int f;
    for (int i = 0; i < img.width(); i++) {
        for (int j = 0; j < img.height(); j++) {
            if(i >= 1 && j >= 1 && i < (img.width() - 1) && j < (img.height() - 1)) {
                filter = (
                         (qreal)qGray(img.pixel(i - 1, j - 1)) * F[0][0] +
                         (qreal)qGray(img.pixel(i, j - 1))     * F[1][0] +
                         (qreal)qGray(img.pixel(i + 1, j - 1)) * F[2][0] +

                         (qreal)qGray(img.pixel(i - 1, j)) * F[0][1] +
                         (qreal)qGray(img.pixel(i, j))     * F[1][1] +
                         (qreal)qGray(img.pixel(i + 1, j)) * F[2][1] +

                         (qreal)qGray(img.pixel(i - 1, j + 1)) * F[0][2] +
                         (qreal)qGray(img.pixel(i, j + 1))     * F[1][2] +
                         (qreal)qGray(img.pixel(i + 1, j + 1)) * F[2][2] ) / pf;
                f = qRound(filter);
                newImage.setPixel(i, j, qRgb(f, f, f));
            } else {
                newImage.setPixel(i, j, img.pixel(i, j));
            }
        }
    }
    return newImage;
}
int MyImagesOp::widthMyImage() const {
    return imageOR.width();
}
int MyImagesOp::heightMyImage() const {
    return imageOR.height();
}
int MyImagesOp::maxArray(int *A, int Linf, int Lsup) {
    if( (Lsup - Linf) <= 1) {
        if(A[Linf] < A[Lsup]) {
           return A[Lsup];
        } else {
           return A[Linf];
        }
     } else {
        int m = (Linf + Lsup)/2;
        int max1 = maxArray(A, Linf, m);
        int max2 = maxArray(A, ( m + 1 ), Lsup);
        if(max1 > max2) {
            return max1;
        } else {
            return max2;
        }
    }
}

int MyImagesOp::minIndexArray(int *A, int Linf, int Lsup) {
    int i = Linf, minV = A[i], minI = i;
    while(i <= Lsup) {
        if(A[i] != 0 && A[i] < minV) {
            minV = A[i];
            minI = i;
        }
        i++;
    }
    return minI;
}
bool MyImagesOp::isRGBMyImage() {
    return rgb;
}
QImage MyImagesOp::scaleToWidget(QImage image_in, int width, int height) {
    int maxS = qMax(image_in.width(), image_in.height());
    int scaled = height;
    double por = (maxS * 100)/scaled;

    int sw = 0, sh = 0;
    if(maxS == image_in.width()){
        sw = scaled;
        sh = (image_in.height() * 100)/por;
    }else{
        sw = (image_in.width() * 100)/por;
        sh = scaled;
    }
    return image_in.scaled(sw, sh);
}

//Segment // gray scale image//
int ** MyImagesOp::borderMyImage(QImage img, int desLeft, int desRight) {
    int gray1, gray2, r1, r2, height = img.height(), width = img.width();
    int ** border  = new int*[height];
    for (int i = 0; i < height; i++) {
        border[i] = new int[2];
    }

    int m = qRound((float)width / 2), point_T1 = -1, point_T2 = -1;
    int kl = 0, kr = width - 1;

    for (int j = 0; j < height; j++) {
        border[j][0] = -1;  border[j][1] = -1;
        while(kl < m) {
            gray1 = qGray( img.pixel(kl, j) );
            gray2 = qGray( img.pixel(kl + 1, j) );
            r1 = qAbs(gray1 - gray2);

            if(r1 >= desLeft && point_T1 == -1) {
                point_T1 = kl + 1;
                border[j][0] = point_T1;
            }

            gray1 = qGray( img.pixel(kr, j) );
            gray2 = qGray( img.pixel(kr - 1, j) );
            r2 = qAbs(gray1 - gray2);

            if(r2 >= desRight && point_T2 == -1) {
                point_T2 = kr - 1;
                border[j][1] = point_T2;
            }

            kl++, kr--;
        }
        kl = 0; kr = width - 1;
        point_T1 = -1;
        point_T2 = -1;
    }
    return border;
}
QImage MyImagesOp::segmentMyImage(QImage img, int desLeft, int desRight) {
    QImage newImage(img.width(), img.height(), img.format());
    for (int i = 0; i < img.width(); i++) {
        for(int j = 0; j < img.height(); j++) {
            newImage.setPixel(i, j, qRgb(255, 255, 255));
        }
    }
    int ** border = borderMyImage(img, desLeft, desRight);
    int aux;
    for (int var = 0; var < img.height(); var++) {
        if(border[var][0] != -1 && border[var][1] != -1) {
            aux = border[var][0];
            while(aux <= border[var][1]) {
                newImage.setPixel(aux, var, qRgb(0, 0, 0));
                aux++;
            }
        }
    }

    for (int i = 0; i < img.height(); i++) {
        delete border[i];
    }
    delete []border;

    return newImage;
}

// reduceHistogram _if length(histogram)%reduction = 0
// length(histogran) = 256
int * MyImagesOp::reduceHistogram(int *histogram, int reduction) {
    int i = 0, len = 256 / reduction, v1 = 0, v2 = reduction - 1,
        total = 0;
    int * histogram_reduction = new int[reduction];
    while(i < len) {
        total = sumArray(histogram, v1, v2);
        histogram_reduction[i] = qRound((float)total / (float)reduction);
        v1 += reduction;
        v2 += reduction;

        i++;
    }
    return histogram_reduction;
}

int MyImagesOp::sumArray(int *array, int first, int last) {
    int total = 0, i = first;
    while(i <= last) {
        total += array[i];
        i++;
    }
    return total;
}

//images(img1.width = img2.width and img1.height = img2.height)
int MyImagesOp::compareMyImages(QImage com, int partitions, int reduction) {
    QList <QuadrantImg> listImg_1 = quadrantsImage(imageE2, partitions);
    QList <QuadrantImg> listImg_2 = quadrantsImage(com, partitions);

    int i = 0, j = 0, len = listImg_1.length(), res = 0, d = 0;
    int diff[len];
    QuadrantImg q;
    while(i < len) {
        q = listImg_1.at(i);
        int * histogram_1 = histogramImage(imageE2, q.first_width, q.last_width, q.first_height, q.last_height);
        int * histogram16_1 = reduceHistogram(histogram_1, reduction);

        q = listImg_2.at(i);
        int * histogram_2 = histogramImage(com, q.first_width, q.last_width, q.first_height, q.last_height);
        int * histogram16_2 = reduceHistogram(histogram_2, reduction);

        j = 0; d = 0;
        while(j < reduction) {
            res = qAbs(histogram16_1[j] - histogram16_2[j]);
            if(res < 4) {
                d++;
            }
            j++;
        }

        if(d >= (reduction - 2)) {
            diff[i] = 1;
        } else {
            diff[i] = 0;
        }

        i++;
        delete []histogram_1; delete []histogram16_1;
        delete []histogram_2; delete []histogram16_2;
    }
    return sumArray(diff, 0, len - 1) == len;
}

int MyImagesOp::compareMyImages_Re(QImage com) {
    int *histogram_1 = histogramImage(imageE2, 0, 0, imageE2.width() - 1, imageE2.height() - 1);

    int *histogram_2 = histogramImage(com, 0, 0, com.width() - 1, com.height() - 1);

    qreal t_img_1 = imageE2.width() * imageE2.height(),
        t_img_2 = com.width() * com.height(),

            v_img = (t_img_1 > t_img_2) ? t_img_2 : t_img_1,
            v_res_1 = v_img / t_img_1,
            v_res_2 = v_img / t_img_2;

    int i = 0, len = 256,
        v_t1, v_t2, aux;
    int *des = new int[256];
    // int s
    int sil = 85, error = 25;
    while(i < len) {
        v_t1 = (qreal)histogram_1[i] * v_res_1;
        v_t2 = (qreal)histogram_2[i] * v_res_2;
        aux = qAbs(v_t1 - v_t2);
        des[i] = 0;
        if(aux < sil) {
            des[i] = 1;
        }
        i++;
    }
    int sumA = sumArray(des, 0, 255);
    qDebug() << sumA;
    int res = ( sumA > (len - error)) ? 1 : 0;

    delete []des;
    delete []histogram_1;
    delete []histogram_2;
    return res;
}

// img : gray scale image
QSizeF MyImagesOp::dimensionMyImage(QImage img,
                                   int desLeft, int desRight, int width_real, int height_real) {
    // border[w][2]
    int ** border = borderMyImage(img, desLeft, desRight);

    int i = 0, j = img.height() - 1,
        len = qRound((float)img.height() / 2);
    int distance_m  = 0,
        d, j_first = 0, j_last = 0;
    while(i < len) {
        if(border[i][0] != -1 && border[i][1] != -1) {
            if (j_first == 0) {
                j_last = j;
            }
            d = qAbs(border[i][0] - border[i][1]);
            if(d > distance_m) {
                distance_m = d;
            }
        }

        if(border[j][0] != -1 && border[j][1] != -1) {
            if (j_last == 0) {
                j_last = j;
            }
            d = qAbs(border[j][0] - border[j][1]);
            if(d > distance_m) {
                distance_m = d;
            }
        }

        i++; j--;
    }

    for (int i = 0; i < img.height(); i++) {
        delete border[i];
    }
    delete []border;

    int di_h = qAbs(j_first - j_last);
    qreal w = (distance_m * width_real) / img.width(),
          h = (di_h * height_real) / img.height();
    return QSizeF(w, h);
}

int MyImagesOp::levelColorQuadrant(QImage segment_img, int color_seg, int iw, int fw, int ih, int fh) {
    int levelColor = 0; QRgb p, color_rgb = qRgb(color_seg, color_seg, color_seg);
    for (int i = iw; i <= fw; i++) {
        for (int j = ih; j <= fh; j++) {
            p = segment_img.pixel(i, j);
            if(p == color_rgb) {
                levelColor++;
            }
        }
    }
    return levelColor;
}
QString MyImagesOp::determineMyMov(QImage img, int partitions, int desLeft, int desRight) {
    QList <QuadrantImg> listImg_1 = quadrantsImage(imageE2, partitions);
    QList <QuadrantImg> listImg_2 = quadrantsImage(img, partitions);

    QImage img_seg_1 = segmentMyImage(imageE2, desLeft, desRight);
    QImage img_seg_2 = segmentMyImage(img, desLeft, desRight);
    // 9 quadrants
    int i = 0, len = listImg_1.length();
    QuadrantImg q;
    int nivel_B_1, nivel_B_2;
    int l1[9];
    int l2[9];
    while(i < len) {
        q = listImg_1.at(i);
        nivel_B_1 = levelColorQuadrant(img_seg_1, 0, q.first_width, q.last_width, q.first_height, q.last_height);
        //break;

        q = listImg_2.at(i);
        nivel_B_2 = levelColorQuadrant(img_seg_2, 0, q.first_width, q.last_width, q.first_height, q.last_height);

        l1[i] = nivel_B_1;
        l2[i] = nivel_B_2;

        qDebug() << i << ": " << nivel_B_1  << nivel_B_2;

        i++;
    }

    QString mov = "Ninguno";
    // left
    if( l1[1] < l2[1] ) {
        mov = "Izquierda";
    }

    // right
    else if( l1[7] < l2[7] ) {
        mov = "Derecha";
    }

    // top
    else if( l1[3] < l2[3] ) {
        mov = "Arriba";
    }

    // bottom
    else if( l1[5] < l2[5] ) {
        mov = "Abajo";
    }
    qDebug() << mov << "\n" << " --------- ";
    return mov;
}
//

#ifndef MYIMAGESOP_H
#define MYIMAGESOP_H

#include <QImage>
#include <QDebug>
#include <QList>
#include <QSize>

// Quadrant Image
class QuadrantImg;

class MyImagesOp {
public:
    MyImagesOp(QString url_image="");
    void loadMyImage(QString url_image);

    // images
    QImage orImage();
    QImage grayE1Image();
    QImage grayE2Image();
    QImage binarizedImage(QImage img_in, int umbral);
    int * histogramImage(QImage img, int iw, int ih, int fw, int fh);
    int umbralImageRegion(int * histogram);
    // binarized local
    QImage binarized_P_Image(QImage img_in, int p);

    //
    QImage filterImage(QImage img, qreal F[3][3], qreal pf);
    // dimension img
    int widthMyImage() const;
    int heightMyImage() const;
    // aditional
    int maxArray(int * A, int Linf, int Lsup);
    int minIndexArray(int * A, int Linf, int Lsup);
    bool isRGBMyImage();
    QImage scaleToWidget(QImage image_in, int width, int height);
    //
    void binarized_rect(QImage & image_in, int iw, int ih, int fw, int fh, int umbral);
    // quadrants image
    QList<QuadrantImg> quadrantsImage(QImage img_in, int partitions);
    // op aditional // standart image //
    int ** borderMyImage(QImage img, int desLeft, int desRight);
    QImage segmentMyImage(QImage img, int desLeft, int desRight);
    // reduce histogram: buckets
    int * reduceHistogram(int * histogram, int reduction);
    int sumArray(int * array, int first, int last);
    // compare
    int compareMyImages(QImage com, int partitions, int reduction);
    int compareMyImages_Re(QImage com);
    // dimension
    QSizeF dimensionMyImage(QImage img, int desLeft, int desRight, int width_real, int height_real);
    //
    int levelColorQuadrant(QImage segment_img, int color_seg, int iw, int fw, int ih, int fh);
    QString determineMyMov(QImage img, int partitions, int desLeft, int desRight);

    // ------------
private:
    QImage imageOR,
    imageE1, imageE2;
    bool rgb;
};

class QuadrantImg {
public:
    int first_width, last_width;
    int first_height, last_height;

    void print() {
        qDebug() << "Q[(" << first_width << ", " << first_height << "), ("
               << last_width << ", " << last_height << ")]";
    }
};



#endif // MYIMAGESOP_H

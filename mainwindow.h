#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStatusBar>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>

#include <QGraphicsView>
#include <QGraphicsScene>

#include <QGraphicsPixmapItem>
#include <QGraphicsRectItem>
#include <QGraphicsTextItem>

#include "myimagesop.h"

// GUI
class MainWindow : public QMainWindow {
    Q_OBJECT
    
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    // methods
    void initMenuBar();
    void showLeftRightImages(QString titleLeft, QImage imgLeft,
                             QString titleRight, QImage imgRight);
    void showLeftTitle(QString titleLeft);
    void showRightTitle(QString titleRight, int w);

    void showLeftImage(QImage imgLeft);
    void showRightImage(QImage imgRight);

    //
    void myBinarizing(int umbral);

    //
    // properties
    // wiget
    void resetScene();

    QGraphicsView graphicsView;
    QGraphicsScene *graphicsScene;
    // var
    MyImagesOp imageops;
    QMenuBar menuBar;
    QMenu menuApp, menuImage, menuBinarizing, menuHistogram, menuFilters,
    menuCompare, menuAdditional;
    // menu app
    QAction *actionLoadImage, *actionQuit;
    // menu image
    QAction *actionGrayScale,
    // actios bynarized
     *actionBUmbral, *actionBGlobal, *actionBLocal,
    // histogram
     *actionHist_RGB, *actionHist_GrayScale,
    // filters
     *actionFilterPro, *actionFilterGauss,
    // Compare
    *actionCompare_Cuad, *actionCompare_Hist,
    // additional
    *actionSegment,  *actionDimension, *actionMov;
    // Status bar
    QStatusBar statusBar;
private slots:
    void slotLoadImage();
    void slotGrayScale();
    // binarizing
    void slotBUmbral();
    void slotBGlobal();
    void slotBLocal();
    // histogram
    void slotHist_RGB();
    void slotHist_GrayScale();
    // filters
    void slotFilterPro();
    void slotFilterGauss();
    // compare
    void slotCompare_Cuad();
    void slotCompare_Hist();
    // additional
    void slotSegment();
    void slotDimension();
    void slotMov();
};

#endif // MAINWINDOW_H

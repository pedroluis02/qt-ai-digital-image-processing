#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent) {
    initMenuBar();
    graphicsView.setParent(this);
    graphicsScene = new QGraphicsScene(&graphicsView);
    graphicsView.setScene(graphicsScene);

    statusBar.setParent(this);

    setCentralWidget(&graphicsView);

    setStatusBar(&statusBar);

    imageops.loadMyImage(QString::fromUtf8(":/resources/laserkeybord.jpg"));
    graphicsScene->addPixmap(QPixmap::fromImage( imageops.scaleToWidget( imageops.orImage(), 0, 600 ) ));
}
void MainWindow::initMenuBar() {
    menuBar.setParent(this);

    // menu App
    //menuApp.setParent(&menuBar); // Error KDE
    menuApp.setTitle("&Application");

    actionLoadImage = new QAction("Load Image ...", &menuApp);
    actionQuit = new QAction("Quit", &menuApp);

    connect(actionQuit, SIGNAL(triggered()), this, SLOT(close()));
    connect(actionLoadImage, SIGNAL( triggered() ), this, SLOT( slotLoadImage() ));

    menuApp.addAction(actionLoadImage); menuApp.addSeparator();
    menuApp.addAction(actionQuit);

    // menu Image
    //menuImage.setParent(&menuBar);
    menuImage.setTitle("&Image");

    actionGrayScale = new QAction("Gray Scale", &menuImage);
    menuBinarizing.setTitle("Binarization");
    //menuBinarizing.setParent(&menuImage);
       actionBUmbral = new QAction("Umbral", &menuBinarizing);
       actionBGlobal = new QAction("Global", &menuBinarizing);
       actionBLocal = new QAction("Local", &menuBinarizing);

    connect(actionGrayScale, SIGNAL( triggered() ), this, SLOT( slotGrayScale() ));
       connect(actionBUmbral, SIGNAL( triggered() ), this, SLOT( slotBUmbral() ));
       connect(actionBGlobal, SIGNAL( triggered() ), this, SLOT( slotBGlobal() ));
       connect(actionBLocal, SIGNAL( triggered() ), this, SLOT( slotBLocal() ));

       menuBinarizing.addAction(actionBUmbral);
       menuBinarizing.addAction(actionBGlobal);
       menuBinarizing.addAction(actionBLocal);

    // menu Histogram
    //menuHistogram.setParent(&menuBar);
    menuHistogram.setTitle("&Histogram");

    actionHist_RGB = new QAction("RGB", &menuHistogram);
    actionHist_GrayScale = new QAction("Gray Scale", &menuHistogram);

    connect(actionHist_RGB, SIGNAL( triggered() ), this, SLOT( slotHist_RGB() ));
    connect(actionHist_GrayScale, SIGNAL( triggered() ), this, SLOT( slotHist_GrayScale() ));

    menuHistogram.addAction(actionHist_RGB);
    menuHistogram.addAction(actionHist_GrayScale);

    // menu filters
    //menuFilters.setParent(&menuBar);
    menuFilters.setTitle("&Filters");

    actionFilterPro = new QAction("Average", &menuFilters);
    actionFilterGauss = new QAction("Gauss", &menuFilters);

    connect(actionFilterPro, SIGNAL( triggered() ), this, SLOT( slotFilterPro() ));
    connect(actionFilterGauss, SIGNAL( triggered() ), this, SLOT( slotFilterGauss() ));

    menuFilters.addAction(actionFilterPro); menuFilters.addAction(actionFilterGauss);

    menuImage.addAction(actionGrayScale); menuImage.addMenu(&menuBinarizing);
    // menu compare
    //menuCompare.setParent(&menuBar);
    menuCompare.setTitle("&Compare");

    actionCompare_Cuad = new QAction("Quadrants(Buckets)", &menuCompare);
    actionCompare_Hist = new QAction("Normalized(Histogram)", &menuCompare);

    connect(actionCompare_Cuad, SIGNAL(triggered()), this, SLOT(slotCompare_Cuad()));
    connect(actionCompare_Hist, SIGNAL(triggered()), this, SLOT(slotCompare_Hist()));

    menuCompare.addAction(actionCompare_Cuad); menuCompare.addAction(actionCompare_Hist);

    // menu Additional
    //menuAdditional.setParent(&menuBar);
    menuAdditional.setTitle("&Extra");

    actionSegment = new QAction("Segment", &menuAdditional);

    actionDimension = new QAction("Dimension", &menuAdditional);
    actionMov = new QAction("Is moving....", &menuAdditional);

    connect(actionSegment, SIGNAL(triggered()), this, SLOT(slotSegment()));
    connect(actionDimension, SIGNAL(triggered()), this, SLOT(slotDimension()));
    connect(actionMov, SIGNAL(triggered()), this, SLOT(slotMov()));

    menuAdditional.addAction(actionSegment);
    menuAdditional.addAction(actionDimension);
    menuAdditional.addAction(actionMov);

    // menus
    menuBar.addMenu(&menuApp); menuBar.addMenu(&menuImage);
    menuBar.addMenu(&menuHistogram); menuBar.addMenu(&menuFilters);
    menuBar.addSeparator(); menuBar.addMenu(&menuCompare); menuBar.addMenu(&menuAdditional);

     setMenuBar(&menuBar);
}

void MainWindow::showLeftRightImages(QString titleLeft, QImage imgLeft,
                                     QString titleRight, QImage imgRight) {
    graphicsScene->clear();
    //resetScene();

    showLeftTitle(titleLeft); showLeftImage(imgLeft);
    showRightTitle(titleRight, imgLeft.width()); showRightImage(imgRight);
}
void MainWindow::showLeftTitle(QString titleLeft) {
    QGraphicsTextItem *i1 = graphicsScene->addText(titleLeft);
    i1->setDefaultTextColor(Qt::blue);
    QFont f = i1->font(); f.setBold(true); f.setUnderline(true); i1->setFont(f);
}
void MainWindow::showRightTitle(QString titleRight, int w) {
    QGraphicsTextItem *i2 = graphicsScene->addText(titleRight);
    i2->setDefaultTextColor(Qt::red);
    i2->setX(w + 10); QFont f = i2->font();
    f = i2->font();f.setBold(true); f.setUnderline(true); i2->setFont(f);
}

void MainWindow::showLeftImage(QImage imgLeft) {
    QGraphicsPixmapItem *pe1 =  graphicsScene->addPixmap(QPixmap::fromImage(imgLeft));
    pe1->setY(30);

    graphicsScene->addRect(pe1->boundingRect(), QPen(Qt::blue))
    ->setY(30);
}
void MainWindow::showRightImage(QImage imgRight) {
    QGraphicsPixmapItem *pe2 = graphicsScene->addPixmap(QPixmap::fromImage(imgRight));
    pe2->setPos(imgRight.width() + 10, 30);

    QGraphicsRectItem *r2 = graphicsScene->addRect(pe2->boundingRect(), QPen(Qt::red));
    r2->setPos(imgRight.width() + 10, 30);
}

void MainWindow::myBinarizing(int umbral) {
    graphicsScene->clear();
    //resetScene();

    QImage imgb = imageops.scaleToWidget(imageops.grayE2Image(), 0, 600);
    showLeftTitle("Imagen en Escala de Grises");
    showLeftImage(imgb);

    QImage imgB = imageops.binarizedImage(imageops.grayE2Image(), umbral);
    showRightTitle("Imagen Binarizada: umbral[ " + QString::number(umbral) + QString(" ]"),
                   imgb.width()); showRightImage( imageops.scaleToWidget( imgB, 0, 600)  );
}

void MainWindow::resetScene() {
    QGraphicsScene *aux = graphicsScene;
    graphicsScene = new QGraphicsScene(&graphicsView);
    delete aux;
    graphicsView.setScene(graphicsScene);
}

// slots
void MainWindow::slotLoadImage() {
    QString url = QFileDialog::getOpenFileName(this, "Image to open", QDir::homePath(),
                                              "Images (*.jpeg *.jpg *.bmp)");
    if(url.isEmpty()) {
        return;
    }
    imageops.loadMyImage(url);

    resetScene();

    graphicsScene->addPixmap(QPixmap::fromImage( imageops.scaleToWidget( imageops.orImage(), 0, 600 ) ));
}
void MainWindow::slotGrayScale() {
    showLeftRightImages("Escalado a Gris por Promedio",  imageops.scaleToWidget( imageops.grayE1Image(), 0 , 600),
                         "Escalado a Gris por Porcentaje", imageops.scaleToWidget( imageops.grayE2Image(), 0, 600) );
}
void MainWindow::slotBUmbral() {
    bool ok;
    int umbral = QInputDialog::getInt(this, tr("Input Umbral"),
                                      tr("Umbral: "), 128, 5, 250, 1, &ok);
    if (!ok) {
        return;
    }

    myBinarizing(umbral);
}
void MainWindow::slotBGlobal() {
    int umbral = imageops.umbralImageRegion( imageops.histogramImage(imageops.grayE2Image(), 0, 0, imageops.widthMyImage() - 1,
                                                                     imageops.heightMyImage() - 1) );
    myBinarizing(umbral);
}
void MainWindow::slotBLocal() {
    graphicsScene->clear();

    QImage imgb = imageops.scaleToWidget(imageops.grayE2Image(), 0, 600);
    showLeftTitle("Imagen en Escala de Grises");
    showLeftImage(imgb);

    QImage imgB = imageops.binarized_P_Image(imageops.grayE2Image(), 6);
    showRightTitle("Imagen Binarizada: Local",
                   imgb.width()); showRightImage( imageops.scaleToWidget( imgB, 0, 600)  );

}
void MainWindow::slotHist_RGB() {

    if(!imageops.isRGBMyImage()) {
        QMessageBox::warning(this, "Histogram RGB", "Imagen esta en escala de grises.");
        return;
    }

    graphicsScene->clear();
    //resetScene();

    showLeftTitle("Imagen original en RGB");
    //
    QImage orImage = imageops.scaleToWidget(imageops.orImage(), 0, 600);
    //
    showLeftImage(orImage);

    showRightTitle("Histograma de Imagen RGB", orImage.width());

    int * h = imageops.histogramImage(imageops.orImage(), 0, 0, imageops.widthMyImage() - 1,
                                      imageops.heightMyImage() - 1);
    qreal ta = (qreal)(256 * 3);
    qreal maxY = (qreal)imageops.maxArray(h, 0, ta - 1);

    qreal lengthY = (qreal)orImage.height();

    qreal maxX = (qreal)orImage.width() / ta;
    qreal x = -maxX, y, xex;
    int i = 0;

    graphicsScene->addLine(0, 0, 0, orImage.height(), QPen(Qt::white))
            ->setPos(-1.5, 35);
    QColor color; int it = 0;
    while(i < ta) {
        xex = x + maxX;
        y = (((qreal)h[i] * lengthY) / maxY);

        if(i <= 255) {
            color = QColor(it, 0, 0);
        } else if(i <= 511) {
            color = QColor(0, it, 0);
        } else {
            color = QColor(0, 0, it);
        }

        if(y >= 0.0) {
            graphicsScene->addRect(0, 0, maxX, y, QPen(color), QBrush(color))
                    ->setPos(xex + orImage.width() + 10, 30 + (lengthY - y));
        }

        x = xex;
        i++;

        if (it == 255 || it == 511){
            it = -1;
        }
        it++;
    }
    delete []h;
}
void MainWindow::slotHist_GrayScale() {
    graphicsScene->clear();
    //resetScene();

    QImage img = imageops.scaleToWidget(imageops.grayE2Image(), 0, 600);

    showLeftTitle("Histograma de Escalado a Gris por Promedio");
    showRightTitle("Histograma de Escalado a Gris por Porcentaje", img.width());

    int * h1 = imageops.histogramImage(imageops.grayE1Image(), 0, 0, imageops.widthMyImage() - 1,
                                       imageops.heightMyImage() - 1);
    int * h2 = imageops.histogramImage(imageops.grayE2Image(), 0, 0, imageops.widthMyImage() - 1,
                                       imageops.heightMyImage() - 1);
    qreal maxY1 = (qreal)imageops.maxArray(h1, 0, 255);
    qreal maxY2 = (qreal)imageops.maxArray(h2, 0, 255);

    qreal lengthY = (qreal)img.height();

    qreal maxX = (qreal)img.width() / (qreal)256;
    qreal x1 = -maxX, y1, xex1;
    qreal x2 = -maxX, y2, xex2;
    int i = 0;

    graphicsScene->addLine(0, 0, 0, img.height(), QPen(Qt::white))->setY(35);

    while(i < 256) {
        xex1 = x1 + maxX;
        y1 = (((qreal)h1[i] * lengthY) / maxY1);
        if(y1 >= 0.0) {
            graphicsScene->addRect(0, 0, maxX, y1, QPen(Qt::black), QBrush(Qt::black))
                    ->setPos(xex1, 30 + (lengthY - y1));
        }

        xex2 = x2 + maxX;
        y2 = (((qreal)h2[i] * lengthY) / maxY2);
        if(y2 >= 0.0) {
            graphicsScene->addRect(0, 0, maxX, y2, QPen(Qt::black), QBrush(Qt::darkGray))
                    ->setPos(xex2 + img.width() + 10, 30 + (lengthY - y2));
        }

        x1 = xex1;
        x2 = xex2;

        i++;
    }
    delete []h1;
    delete []h2;
}
void MainWindow::slotFilterPro() {
    qreal filtro[3][3];
    filtro[0][0]=1; filtro[1][0]=1; filtro[2][0]=1;
    filtro[0][1]=1; filtro[1][1]=2; filtro[2][1]=1;
    filtro[0][2]=1; filtro[1][2]=1; filtro[2][2]=1;

    QImage imgf = imageops.filterImage(imageops.grayE2Image(), filtro, 10);
    showLeftRightImages("Imagen en Escala de Grises", imageops.scaleToWidget( imageops.grayE2Image(), 0, 600),
                        "Imagen con filtro Promedio", imageops.scaleToWidget( imgf, 0, 600 ));
}
void MainWindow::slotFilterGauss() {
    qreal filtro[3][3];
    filtro[0][0]=1; filtro[1][0]=2; filtro[2][0]=1;
    filtro[0][1]=2; filtro[1][1]=4; filtro[2][1]=2;
    filtro[0][2]=1; filtro[1][2]=2; filtro[2][2]=1;

    QImage imgf = imageops.filterImage(imageops.grayE2Image(), filtro, 16);
    showLeftRightImages("Imagen en Escala de Grises", imageops.scaleToWidget( imageops.grayE2Image(), 0, 600),
                        "Imagen con filtro Gaussiano",
                        imageops.scaleToWidget( imgf, 0, 600 ));
}
// compare
void MainWindow::slotCompare_Cuad() {
    QString url = QFileDialog::getOpenFileName(this, "Image to open", QDir::homePath(),
                                              "Images (*.jpeg *.jpg *.bmp)");
    if(url.isEmpty()) {
        return;
    }
    MyImagesOp opAux; opAux.loadMyImage(url);

    graphicsScene->clear();
    //resetScene();

    QImage imgb = imageops.scaleToWidget(imageops.grayE2Image(), 0, 600);
    showLeftTitle("Imagen 1");
    showLeftImage(imgb);

    QImage imgB = opAux.grayE2Image();
    showRightTitle("Imagen 2", imgb.width());
    showRightImage( imageops.scaleToWidget( imgB, 0, 600) );

    int dev = imageops.compareMyImages(opAux.grayE2Image(), 3, 16);
    if(dev) {
        QMessageBox::information(this, "Comparacion", "Iguales");
    } else {
        QMessageBox::information(this, "Comparacion", "Diferentes");
    }
}
void MainWindow::slotCompare_Hist() {
    QString url = //QString::fromUtf8("/home/pedroluis/Imágenes/manzanas-ia/qt3d.jpg")
            QFileDialog::getOpenFileName(this, "Image to open", QDir::homePath(),
                                             "Images (*.jpeg *.jpg *.bmp)")
            ;
    if(url.isEmpty()) {
        return;
    }
    MyImagesOp opAux;
    opAux.loadMyImage(url);

    graphicsScene->clear();

    QImage imgb = imageops.scaleToWidget(imageops.grayE2Image(), 0, 600);
    showLeftTitle("Imagen 1");
    showLeftImage(imgb);

    QImage imgB = opAux.grayE2Image();
    showRightTitle("Imagen 2", imgb.width());
    showRightImage( imageops.scaleToWidget( imgB, 0, 600) );

    int dev = imageops.compareMyImages_Re(opAux.grayE2Image());
    if(dev) {
        QMessageBox::information(this, "Comparacion", "Iguales");
    } else {
        QMessageBox::information(this, "Comparacion", "Diferentes");
    }
}
// ---------------
void MainWindow::slotSegment() {
    graphicsScene->clear();
    //resetScene();

    QImage imgb = imageops.scaleToWidget(imageops.grayE2Image(), 0, 600);
    showLeftTitle("Imagen en Escala de Grises");
    showLeftImage(imgb);

    QImage imgB = imageops.segmentMyImage(imageops.grayE2Image(), 9, 9);
    showRightTitle("Imagen Segmentada",
                   imgb.width()); showRightImage( imageops.scaleToWidget( imgB, 0, 600) );
}
void MainWindow::slotDimension() {
    graphicsScene->clear();
    QImage imgb = imageops.scaleToWidget(imageops.grayE2Image(), 0, 600);
    showLeftTitle("Imagen en Escala de Grises");
    showLeftImage(imgb);

    QImage imgB = imageops.segmentMyImage(imageops.grayE2Image(), 9, 9);
    showRightTitle("Imagen en deteccion de borde",
                   imgb.width()); showRightImage( imageops.scaleToWidget( imgB, 0, 600)  );

    bool ok;
    int w_real = QInputDialog::getInt(this, tr("Input Ancho real"),
                                      tr("Ancho: "), imageops.widthMyImage(), 1, 10000, 1, &ok);
    if (!ok) {
        return;
    }

    int h_real = QInputDialog::getInt(this, tr("Input Alto real"),
                                      tr("Alto: "), imageops.heightMyImage(), 1, 10000, 1, &ok);
    if (!ok) {
        return;
    }

    QSizeF d = imageops.dimensionMyImage(imageops.grayE2Image(), 9, 9, w_real, h_real);
    QMessageBox::information(this, "Dimension real", "(" + QString::number(d.width()) +
                             QString(", ") + QString::number(d.height()) + ")");
}
void MainWindow::slotMov() {
    QString url = QFileDialog::getOpenFileName(this, "Image to open", QDir::homePath(),
                                              "Images (*.jpeg *.jpg *.bmp)");
    if(url.isEmpty()) {
        return;
    }
    MyImagesOp opAux; opAux.loadMyImage(url);

    QImage imgb = imageops.scaleToWidget(imageops.grayE2Image(), 0, 600);
    showLeftTitle("Imagen 1");
    showLeftImage(imgb);

    QImage imgB = opAux.grayE2Image().copy();
    showRightTitle("Imagen 2", imgb.width());
    showRightImage( imageops.scaleToWidget( imgB, 0, 600) );

    QString mov = imageops.determineMyMov(opAux.grayE2Image(), 3, 15, 15);

    QMessageBox::information(this, "Mivimiento en la imagen", mov);
}
//
MainWindow::~MainWindow() {
    delete graphicsScene;
    delete actionLoadImage; delete actionQuit;
    delete actionGrayScale; delete actionBGlobal; delete actionBLocal; delete actionBUmbral;
    delete actionFilterPro; delete actionFilterGauss;
    delete actionCompare_Cuad;  delete actionCompare_Hist;
    delete actionSegment; delete actionDimension; delete actionMov;
}

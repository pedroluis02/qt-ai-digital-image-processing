#-------------------------------------------------
#
# Project created by QtCreator 2013-05-23T10:29:04
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = IAProcImages
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    myimagesop.cpp

HEADERS  += mainwindow.h \
    myimagesop.h

RESOURCES += \
    resources.qrc

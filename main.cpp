#include <QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.setWindowTitle("IA - Digital image processing");
    w.setMinimumSize(900, 600);
    w.showMaximized();
    
    return a.exec();
}
